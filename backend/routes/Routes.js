const express = require("express");
const db = require("../db");
const respUtil = require("../utils");

const router = express.Router();

router.get("/getmovie", (request, response) => {
  const { movie_title } = request.body;

  const query = `select * from movie where movie_title = '${movie_title}'`;

  db.execute(query, (error, result) => {
    response.send(respUtil.createResult(error, result));
  });
});

router.post("/addmovie", (request, response) => {
  const { movie_title, movie_release_date, movie_time, director_name } =
    request.body;

  const query = `insert into movie value
    ('default','${movie_title}', '${movie_release_date}','${movie_time}','${director_name}')`;

  db.execute(query, (error, result) => {
    response.send(respUtil.createResult(error, result));
  });
});

router.post("/updatemovie", (request, response) => {
  const { movie_release_date, movie_time, movie_title } = request.body;

  const query = `update movie set movie_time='${movie_time}' and movie_release_date='${movie_release_date}' where movie_title='${movie_title}'`;

  db.execute(query, (error, result) => {
    response.send(respUtil.createResult(error, result));
  });
});

router.post("/deletemovie", (request, response) => {
  const { movie_title } = request.body;

  const query = `delete from movie where movie_title='${movie_title}'`;

  db.execute(query, (error, result) => {
    response.send(respUtil.createResult(error, result));
  });
});

module.exports = router;
